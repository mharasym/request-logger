![img.png](img.png)

# Log request and response

This package adds a middleware which can log incoming requests and responses. You can see the logs in view panel at `https://your.domain/request-logs`

## Laravel Installation

You can install the package via composer:

```bash
composer require hryha/request-logger
```

You must publish the asset files with:

```bash
php artisan vendor:publish --provider="Hryha\RequestLogger\RequestLoggerServiceProvider" 
```

You must publish the config file and assets with:

```php
return [
    'locale' => env('REQUEST_LOGGER_LOCALE', 'en'), // available 'en', 'ua'
    'middleware' => [
        \Hryha\RequestLogger\Http\Middleware\BaseAuth::class,
    ],
    'base_auth' => [
        'login' => env('REQUEST_LOGGER_LOGIN', 'login'),
        'password' => env('REQUEST_LOGGER_PASSWORD', 'password'),
    ],
    'timezone' => env('REQUEST_LOGGER_TIMEZONE', env('APP_TIMEZONE', config('app.timezone', 'UTC'))),
    'date_format' => env('REQUEST_LOGGER_DATE_FORMAT', 'Y-m-d'),
    'time_format' => env('REQUEST_LOGGER_TIME_FORMAT', 'H:i:s.u'),
    'log_keep_days' => env('REQUEST_LOGGER_KEEP_DAYS', 14),
    'table_name' => 'request_logs',
    'enabled' => env('REQUEST_LOGGER_ENABLED', true),
    'ignore_paths' => [
        'request-logs*',
        'telescope*',
        'nova-api*',
    ],
    'hide_fields' => [
        'request' => [
            'headers' => [
                'authorization',
                'php-auth-user',
                'php-auth-pw',
            ],
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
        'response' => [
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
    ],
    'replacer_hidden_fields' => '|^_-|',
];
```

You have to execute migrations with:

```php
    php artisan migrate --path=/vendor/hryha/request-logger/database/migrations/2021_11_05_000000_create_request_log_fingerprints_table.php
    php artisan migrate --path=/vendor/hryha/request-logger/database/migrations/2021_11_05_000000_create_request_logs_table.php
```

This packages provides a middleware which can be added as a global middleware or as a single route.

```php
// in `app/Http/Kernel.php`

protected $middleware = [
    // ...
    
    \Hryha\RequestLogger\Http\Middleware\RequestLogger::class
];
```

```php
// in a routes file

Route::post('/test', function () {
    //
})->middleware(\Hryha\RequestLogger\Http\Middleware\RequestLogger::class);
```

## Supported drivers

- storage logs
- database (mysql)

## Lumen Installation

You can install the package via composer:

```bash
composer require hryha/request-logger --dev
```

You must install [vendor:publish plugin](https://github.com/laravelista/lumen-vendor-publish)

You must register provider:

```php
//in 'bootstrap/app.php'

$app->register(\Hryha\RequestLogger\RequestLoggerServiceProvider::class); 
```

You must publish the config file and assets with:

```bash
php artisan vendor:publish --provider="Hryha\RequestLogger\RequestLoggerServiceProvider" 
```

This is the contents of the published config file:

```php
return [
    'locale' => env('REQUEST_LOGGER_LOCALE', 'en'), // available 'en', 'ua'
    'middleware' => [
        \Hryha\RequestLogger\Http\Middleware\BaseAuth::class,
    ],
    'base_auth' => [
        'login' => env('REQUEST_LOGGER_LOGIN', 'login'),
        'password' => env('REQUEST_LOGGER_PASSWORD', 'password'),
    ],
    'timezone' => env('REQUEST_LOGGER_TIMEZONE', env('APP_TIMEZONE', config('app.timezone', 'UTC'))),
    'date_format' => env('REQUEST_LOGGER_DATE_FORMAT', 'Y-m-d'),
    'time_format' => env('REQUEST_LOGGER_TIME_FORMAT', 'H:i:s.u'),
    'log_keep_days' => env('REQUEST_LOGGER_KEEP_DAYS', 14),
    'table_name' => 'request_logs',
    'enabled' => env('REQUEST_LOGGER_ENABLED', true),
    'ignore_paths' => [
        'request-logs*',
        'telescope*',
        'nova-api*',
    ],
    'hide_fields' => [
        'request' => [
            'headers' => [
                'authorization',
                'php-auth-user',
                'php-auth-pw',
            ],
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
        'response' => [
            'content' => [
                'password',
                'token',
                'access_token',
            ],
        ],
    ],
    'replacer_hidden_fields' => '|^_-|',
];
```

You must register this config file:

```php
//in 'bootstrap/app.php'

$app->configure('request-logger');
```

You must execute migrations with:

```bash
php artisan migrate
```

You must register middleware:

```php
// in 'bootstrap/app.php'

$app->routeMiddleware([
    // ...
    
    'request-logger' => \Hryha\RequestLogger\Http\Middleware\RequestLogger::class,
]);
```

This packages provides a middleware which can be added as a global middleware or as a single route.

```php
// in a routes file

Route::post('/test', ['uses' => 'TestController@test', 'middleware' => ['request-logger']]);
```

### Scheduler

After using the package for a while you might have recorded a lot of logs. This package provides an artisan
command `php artisan request-logs:clear` to clean the log.

Running this command will result in the deletion of all recorded logs that is older than the number of days specified in
the `log_keep_days` of the config file.

To delete all logs, add the "--all" parameter `php artisan request-logs:clear --all`

You can leverage Laravel's scheduler to run the clear command now and then.

```php
//in app/Console/Kernel.php

protected function schedule(Schedule $schedule)
{
   $schedule->command('request-logs:clear')->daily();
}
```

### Testing

``` bash
php vendor/bin/phpunit
```
